<?php

/**
 * @file
 * uw_vocab_feds_employment_type.features.inc
 */

/**
 * Implements hook_rdf_default_mappings().
 */
function uw_vocab_feds_employment_type_rdf_default_mappings() {
  $schemaorg = array();

  // Exported RDF mapping: feds_employment_type.
  $schemaorg['taxonomy_term']['feds_employment_type'] = array(
    'rdftype' => array(
      0 => 'skos:Concept',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'rdfs:label',
        1 => 'skos:prefLabel',
      ),
    ),
    'description' => array(
      'predicates' => array(
        0 => 'skos:definition',
      ),
    ),
    'vid' => array(
      'predicates' => array(
        0 => 'skos:inScheme',
      ),
      'type' => 'rel',
    ),
    'parent' => array(
      'predicates' => array(
        0 => 'skos:broader',
      ),
      'type' => 'rel',
    ),
  );

  return $schemaorg;
}
