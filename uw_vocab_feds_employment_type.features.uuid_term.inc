<?php

/**
 * @file
 * uw_vocab_feds_employment_type.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function uw_vocab_feds_employment_type_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Volunteer',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '0569a336-d727-4c04-9c51-f23b57ab0d65',
    'vocabulary_machine_name' => 'feds_employment_type',
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'feds-employment-type/volunteer',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Part-Time',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '242ca390-87d4-4190-be42-ff18f9339f01',
    'vocabulary_machine_name' => 'feds_employment_type',
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'feds-employment-type/part-time',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'External',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '26c26586-a595-4c45-9fd9-e575b7f2ed53',
    'vocabulary_machine_name' => 'feds_employment_type',
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'feds-employment-type/external',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Full-Time',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '8685c78f-fa35-41b1-a834-5cc8b2d9deff',
    'vocabulary_machine_name' => 'feds_employment_type',
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'feds-employment-type/full-time',
        'language' => 'und',
      ),
    ),
  );
  return $terms;
}
